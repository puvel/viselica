﻿
class Programm
{

    static void Main(string[] args)
    {
        Console.WriteLine("Hello group\n");
        Thread.Sleep(1000);
        Console.WriteLine("Let's play \"виселица\"\n");
        Thread.Sleep(1000);
        Console.WriteLine("Supercomputer has already generated a word\n");
        Thread.Sleep(1000);
        Console.WriteLine("Try to guess it\n");
        Thread.Sleep(1000);

        string[] words = {  "baloon","machine", "burger", "military", "newspaper" };  // создаём список слов

        Random generator = new Random();

        string word = words[generator.Next(0, words.Length-1)];     // создаём переменную рандома, чтобы сгенерировать позже случайное слово
        //Console.WriteLine($"The word is - {word}\n");

        Thread.Sleep(100);
        int countOfAttempts = 7;              // количество попыток
        bool guessLetterIndicator = false;
        string wordOutput = "";
        string[]wordOutputArray = new string[word.Length];


        for (int i = 0; i < wordOutputArray.Length; i++)         //создали пустышку, которую будем выводить
        { 
            wordOutputArray[i] = "_"; 
        }                        

        string letter = string.Empty;            // переменная букв которые мы будем принимать
        int deathCounter = 0;




        string[] render = 
        {
            "   \n\n \n \n \n \n ===============",
            "   \n     |\n     |\n     |\n     |\n     |\n     |\n ===============",
            "     _________\n     |\n     |\n     |\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |\n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |      /|\\ \n     | \n     |\n ===============",
            "     _________\n     |       |\n     |       |\n     |       0\n     |      /|\\ \n     |      / \\  \n     | \n ===============",
        };



        string[] alreadyGuessedLetters = new string[word.Length];
        bool alreadyGuessedLetersIndicator = false;
        int alreadyGuessedLiettersArrayCounter =0;
        while (true)
        {
            Console.Write("Insert a letter: ");
            letter = Console.ReadLine();
            Console.WriteLine();

            alreadyGuessedLetersIndicator = false;
            guessLetterIndicator = false;
            for (int i = 0; i < word.Length; i++)
            {

                if (Equals(letter, Convert.ToString(word[i])))
                {
                    wordOutputArray[i] = letter;
                    guessLetterIndicator = true;
                    
                }

            }
            if (guessLetterIndicator == true)
            {
                
                for (int i = 0; i < alreadyGuessedLetters.Length; i++)
                {
                    if (letter == alreadyGuessedLetters[i])
                    {
                        alreadyGuessedLetersIndicator = true;
                    }
                }
                if (alreadyGuessedLetersIndicator == false)
                {
                    Console.WriteLine("You guessed the letter\n");
                    alreadyGuessedLetters[alreadyGuessedLiettersArrayCounter] = letter;
                    alreadyGuessedLiettersArrayCounter++;
                }
                else
                {
                    Console.WriteLine("You have already guessed this letter!\n");
                }
            }
            if (guessLetterIndicator == false)
            {
                Console.Write("there are no such letters\n");
                Console.WriteLine(render[deathCounter]);
                deathCounter++;    
            }

            Console.WriteLine("\n----------------------------------------------------------\n");

            Console.Write("Your word is - ");
            for (int i = 0; i < wordOutputArray.Length; i++)
            {
                Console.Write(wordOutputArray[i]);
            }
            Console.WriteLine("\n----------------------------------------------------------\n \n");



            if (deathCounter == countOfAttempts)
            {
                Console.WriteLine("\nOUT OF ATTEMPTS");
                break;             // выход из цикла while 
            }

            wordOutput = string.Join("", wordOutputArray);  //преобразовали массив угаданных букв пользователя в одно слово
            if (Equals(wordOutput , word))
            {
                Console.WriteLine("\nYOU GUESSED THE WORD!");
                break;
            }
        }

        Console.WriteLine("GAME OVER");

        Console.ReadKey();
    }
}